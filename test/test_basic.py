import os
import getpass
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
print(f'Assuming that your kerberos keytab is in the home folder, '
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/'+
          f'{getpass.getuser()}.keytab {getpass.getuser()}');

import json
import nx2pd as nx
import pandas as pd

from nxcals.spark_session_builder import get_or_create, Flavor

logging.info('Creating the spark instance')

# Here I am using YARN (to do compution on the cluster)
#spark = get_or_create(flavor=Flavor.YARN_SMALL, master='yarn')

# Here I am using the LOCAL (assuming that my data are small data,
# so the overhead of the YARN is not justified)
# WARNING: the very first time you need to use YARN
# spark = get_or_create(flavor=Flavor.LOCAL)

logging.info('Creating the spark instance')
spark = get_or_create(flavor=Flavor.LOCAL,
conf={'spark.driver.maxResultSize': '8g',
    'spark.executor.memory':'8g',
    'spark.driver.memory': '16g',
    'spark.executor.instances': '20',
    'spark.executor.cores': '2',
    })
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')

#def test_kerberos_token():


def test_nxcals_df():
    data_list = ["HX:FILLN"]
    t0 = 1634707139157238525
    t1 = 1634751674621363525
    df = sk.nxcals_df(data_list, t0, t1)
    pd_df = df.toPandas()
    assert len(pd_df) == 2  # a sanity check
    assert pd_df.nxcals_value.values[0] == 7505
    assert pd_df.nxcals_value.values[1] == 7504

def test_get_variable():
    aux = sk.get_variables(['LHC.BCTDC%:BEAM_INTENSITY','%HX:BMO%',])
    assert len(aux)==26

def test_get_fill_time():
    my_fill = 6666
    aux = sk.get_fill_time(my_fill)
    print(json.dumps(aux,
                    sort_keys=False, indent=4, default=str))
    assert aux['duration'] == pd.Timedelta('0 days 14:53:58.321000')

def test_get_fill_time_of_the_last():
    my_fill = 'last'
    aux = sk.get_fill_time(my_fill)
    print(json.dumps(aux,
                     sort_keys=False, indent=4, default=str))
    assert aux['end'] < pd.Timestamp.now(tz='UTC')
    assert aux['start'] > pd.Timestamp('2023-10-10', tz='UTC')

def test_latency():
    data_list = ["LHC.BCTDC%:BEAM_INTENSITY", "LHC.BCTFR.A6R4.B%:BUNCH_INTENSITY"]
    # You can use directly pd.Timestamp
    # and mix the different formats
    # Note that all format (pd.Timistamp, int, str) maintains the ns precision
    t1 = pd.Timestamp.now(tz="UTC")
    t0 = t1-pd.Timedelta('1h')
    df = sk.get(t0, t1, data_list)
    print(t1)
    print(len(df))
    df.tail()
    assert len(df) > 1000
