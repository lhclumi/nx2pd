print( 40*'+' + '\n Assuming you have a valid kerberos token.\n' + 40*'+')

import nx2pd as nx
import pandas as pd
from nx2pd import SparkIt
from nxcals.spark_session_builder import get_or_create, Flavor
spark = get_or_create(flavor=Flavor.LOCAL)

sk = SparkIt(spark)

t0 = pd.Timestamp('2022.11.02 08')
t1 = pd.Timestamp('2022.11.02 23')
data_list = [{
              "context": "LHC_BCMS_Q20_2022_V2",
              "parameter": "SPSBEAM/QPH",
            }]

df = sk.nxcals_df(
                  data_list,
                  t0,
                  t1,
                  system_name="SETTINGS",
                 ) 
# converted to pandas for readibility
print(df.toPandas())
