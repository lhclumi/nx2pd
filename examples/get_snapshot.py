# %%
print( 40*'+' + '\n Assuming you have a valid kerberos token.\n' + 40*'+')

import nx2pd as nx
import pandas as pd
from nx2pd import SparkIt
from nxcals.spark_session_builder import get_or_create, Flavor
spark = get_or_create(flavor=Flavor.LOCAL)

sk = SparkIt(spark)

# %%
result = sk.get_snapshot('LBOC 2023_10_10 3')

data_list = result[0]
# You can use directly pd.Timestamp
# and mix the different formats
# Note that all format (pd.Timistamp, int, str) maintains the ns precision
t1 = result[1]['getEndTime']
t0 = result[1]['getStartTime']
if result[1]['getTimeZone']=='LOCAL_TIME':
    t0 = pd.Timestamp(t0).tz_localize('Europe/Zurich')
    t1 = pd.Timestamp(t1).tz_localize('Europe/Zurich')
df = sk.get(t0, t1, data_list + ['TCTPV.4R1.B2:MEAS_LVDT_GD'])
print(t1)
df.tail()

# %%
from matplotlib import pyplot as plt
plt.figure(figsize=(15,10))
plt.subplot(311)
plt.plot(df['HX:BETASTAR_IP1'].dropna().between_time('11:00','23:00',axis=0))
plt.title('FILL 9043'  )
plt.ylabel('BETASTAR_IP1 [cm]')
plt.xticks([])

plt.subplot(312)

plt.plot(df['RPMC.UL16.RBBCW.R1B2:I_MEAS'].dropna().between_time('11:00','23:00',axis=0))
plt.ylabel('B2 wire current [A]')
plt.xticks([])


plt.subplot(313)
plt.plot(df['TCTPH.4R5.B2:MEAS_LVDT_GD'].dropna().between_time('11:00','23:00',axis=0), label = 'TCTPH.4R5.B2')

plt.plot(df['TCTPV.4R1.B2:MEAS_LVDT_GD'].dropna().between_time('11:00','23:00',axis=0), label = 'TCTPV.4R1.B2')
plt.ylabel('downstream gap[mm]')

plt.legend(loc='upper right')
#sk.get(t0-pd.Timedelta('1d'), t1, ['HX:FILLN']).tail()
# %%
# %%
result = sk.get_snapshot('LBOC 2023_10_10 2')

data_list = result[0]
# You can use directly pd.Timestamp
# and mix the different formats
# Note that all format (pd.Timistamp, int, str) maintains the ns precision
t1 = result[1]['getEndTime']
t0 = result[1]['getStartTime']
# if result[1]['getTimeZone']=='LOCAL_TIME':
#     t0 = pd.Timestamp(t0).tz_localize('Europe/Zurich')
#     t1 = pd.Timestamp(t1).tz_localize('Europe/Zurich')
df = sk.get(t0, t1, data_list)
print(t1)
df.tail()

# %%
from matplotlib import pyplot as plt
plt.figure(figsize=(15,10))
plt.subplot(311)
plt.plot(df['RPHRA.RR13.RQ4.L1B2:I_MEAS'].dropna(), label= 'Q4.L1')
plt.plot(df['RPHRA.RR17.RQ4.R1B2:I_MEAS'].dropna(), label= 'Q4.R1')
plt.legend(loc='upper right')
plt.ylabel('Quadrupole Current [A]')

plt.title('FILL 9043')
plt.xticks([])

plt.subplot(312)

plt.plot(df['RPMC.UL16.RBBCW.R1B2:I_MEAS'].dropna())
plt.ylabel('B2 wire current [A]')
plt.xticks([])


plt.subplot(313)

plt.plot(df['RPHRA.RR53.RQ4.L5B2:I_MEAS'].dropna(), label= 'Q4.L5')
plt.plot(df['RPHRA.RR57.RQ4.R5B2:I_MEAS'].dropna(), label = 'Q4.R5')
plt.ylabel('Quadrupole Current [A]')

plt.legend(loc='upper right')

for QQ in ['RPHRA.RR13.RQ4.L1B2:I_MEAS','RPHRA.RR17.RQ4.R1B2:I_MEAS',
           'RPHRA.RR53.RQ4.L5B2:I_MEAS','RPHRA.RR57.RQ4.R5B2:I_MEAS' ]:
    aux = df[QQ].dropna().values
    print(aux)
    print(f'{QQ}: ' + f'{(aux[-1]-aux[0])/aux[0]*100}')