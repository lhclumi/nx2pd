# %% Initialization
# I tested it on pcbe-abp-hpc002 (large memory)
# you can inspect the running application with 
# http://ithdp1001.cern.ch:8088/cluster/apps/RUNNING
# the scheduler info be found at
# http://ithdp1001.cern.ch:8088/cluster/scheduler
from pathlib import Path
import os
import getpass
import logging
import sys
import json
import nx2pd as nx 
import pandas as pd
from datetime import datetime
import subprocess
from copy import deepcopy
import numpy as np

my_path=Path('./')
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()

def has_kerberos_ticket():
    return True if subprocess.call(['klist', '-s']) == 0 else False

if has_kerberos_ticket():
      # the easiest strategy is doing a kinit (this is not possible for batch jobs) 
      print('Valid kerberos ticket found.')
elif  os.path.exists(f'{os.path.expanduser("~")}/{getpass.getuser()}.keytab'):
      # this is the strategy for batch jobs
      print(f'Kerberos keytab {getpass.getuser()}.keytab found in the home folder.')
      logging.info('Executing the kinit')
      os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/{getpass.getuser()}.keytab {getpass.getuser()}')
else:
     raise Exception('No valid kerberos ticket found.')

# %% Start the spark session

logging.info('Importing the spark session builder')
from nxcals.spark_session_builder import get_or_create, Flavor
logging.info('Creating the spark instance')
local = True
logging.info(f'Use Flavor.LOCAL: {local}')
if local:
      spark = get_or_create(flavor=Flavor.LOCAL)
      split_h = 1
      duration_fill_to_split_h = 2
      logging.info(f'The h_split is set to {split_h} hour')
else:
      spark = get_or_create(flavor=Flavor.YARN_LARGE, 
      master='yarn', conf={'spark.driver.maxResultSize': '32g', 
            'spark.driver.memory': '8g',
            'spark.executor.instances': '20',
            'spark.executor.memory':'16g',
            'spark.executor.cores': '4'}
            )
      # for fill longer than 12 hours then we split each 6 hours
      split_h = 6
      duration_fill_to_split_h = 15
      logging.info(f'The h_split is set to {split_h} hour')

sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')

# %%

def is_fill_in_parquet(fill, my_path):
      path_test=my_path/f"HX:FILLN={fill}"
      return path_test.exists()

def list_from_snapshot(snapshot='2023 Lumi Follow-Up'):
      group = sk.group_service.findOne(sk.Groups.suchThat().name().eq(snapshot)).get()
      var_names = [var.getVariableName() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      var_types = [str(var.getDeclaredType()) for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      # to be used in the future
      ########
      var_descriptions = [var.getDescription() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      var_units = [var.getUnit() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]

      my_list = []
      for nn, ii in enumerate(var_names):
            my_list.append({'name':ii, 
                            'types':var_types[nn], 
                            'description':var_descriptions[nn], 
                            'units':var_units[nn]})
      return my_list

def data_list_from_snapshot(snapshot='2023 Lumi Follow-Up'):
      group = sk.group_service.findOne(sk.Groups.suchThat().name().eq(snapshot)).get()
      var_names = [var.getVariableName() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      var_types = [str(var.getDeclaredType()) for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      # to be used in the future
      ########
      var_descriptions = [var.getDescription() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      var_units = [var.getUnit() for var in sk.group_service.getVariables(group.getId())['getSelectedVariables']]
      my_dict = dict(group.getProperties())
      ########

      my_variables = {}

      for ii in np.unique(var_types):
            my_variables[ii] = []
            for jj in range(len(var_names)): 
                  if  var_types[jj] == ii:
                        my_variables[ii].append(var_names[jj])

      my_list = []        
      for ii in my_variables:
            my_list.append(my_variables[ii])

      my_list =  [sorted(my_variables['NUMERIC'])] 
      my_list_1 = deepcopy(my_list)
      my_list_1 = [[ii for ii in my_list_1[0] if not ii.endswith(':ENABLE')]]
      my_list_1 = [[ii for ii in my_list_1[0] if not ii.endswith(':Enable')]]
      my_list_1 = [[ii for ii in my_list_1[0] if not 'HX:AMODE_'  in ii]]


      my_list_2 = deepcopy(my_list)
      my_list_2 = [[ii for ii in my_list[0] if ii.endswith(':ENABLE')]]
      my_list_2 = [my_list_2[0] + [ii for ii in my_list[0] if ii.endswith(':Enable')]]
      my_list_2 = [my_list_2[0] + [ii for ii in my_list[0] if 'HX:AMODE_' in ii]]


      my_list = [my_list_1[0],my_list_2[0], sorted(my_variables['TEXT']), sorted(my_variables['VECTOR_NUMERIC'])]
      
      
      return my_list 


def fill_to_parquet(fill_number, my_path, snapshot='2023 Lumi Follow-Up', delete_and_rewrite=False):
      my_list = data_list_from_snapshot(snapshot)
      if delete_and_rewrite and is_fill_in_parquet(fill_number, my_path):
            os.system(f'rm -rf {my_path}/HX:FILLN={fill_number}')
      if not is_fill_in_parquet(fill_number, my_path):
                  aux = sk.get_fill_time(fill_number)
                  if aux['duration']>pd.Timedelta(f'{duration_fill_to_split_h}h'):
                        my_split_h = split_h
                  else:
                        my_split_h = 100
                  print(json.dumps(aux,
                              sort_keys=False, indent=4,default=str))             
                  my_df = sk.get_fill_data(aux['fill'],
                          events_list=[['HX:FILLN','HX:BMODE']],
                          data_list= my_list,
                          sampling_frequency = None,
                          split_h=my_split_h) 
                  my_df['creation time'] = datetime.utcnow() 
                           
                  # for each column of the my_df
                  for ii in my_df.columns:
                         if ii in my_list[3]:
                              my_df[ii] = my_df[ii].dropna().apply(lambda x : np.array(x['elements']))
                              my_df[ii].fillna(value=np.nan, inplace=True)
                  my_df.loc[aux['start'].value:
                            aux['end'].value].to_parquet(my_path,partition_cols=['HX:FILLN', 'HX:BMODE'])
                  return my_df  
# %%
snapshot_name = '2023 Lumi Follow-Up'
my_list = list_from_snapshot(snapshot=snapshot_name)
my_df = pd.DataFrame(my_list)
print(f'The total number of variables in `{snapshot_name}` is {len(my_df)}')
# sort aux by name
my_df.sort_values(by=['name'], inplace=True)
# %%
# filter aux with name containing 'V_CAVITY'
my_df.loc[my_df['name'].str.contains('V_CAVITY'),'classification'] = 'RF voltage'
my_df.loc[my_df['name'].str.contains('LUMI_'),'classification'] = 'Luminosity'
my_df.loc[my_df['name'].str.contains('Pileup'),'classification'] = 'Luminosity'
my_df.loc[my_df['name'].str.contains('.RBBCW.'),'classification'] = 'BB wire compensators'
my_df.loc[my_df['name'].str.contains('.ROF.'),'classification'] = 'Octupoles'
my_df.loc[my_df['name'].str.contains('.ROD.'),'classification'] = 'Octupoles'
my_df.loc[my_df['name'].str.contains('.BLMDIAMOND'),'classification'] = 'dBLM'

my_df.loc[my_df['name'].str.contains('HX.BETA'),'classification'] = 'Optics'
my_df.loc[my_df['name'].str.contains('BFC.LHC:OpticsActive:opticsName'),'classification'] = 'Optics'
my_df.loc[my_df['name'].str.contains('BEAM_ENERGY'),'classification'] = 'Beam energy'
my_df.loc[my_df['name'].str.contains('HX:FILLN'),'classification'] = 'Fill number'
my_df.loc[my_df['name'].str.contains('HX:AMODE'),'classification'] = 'Modes of operation'
my_df.loc[my_df['name'].str.contains('HX:BMODE'),'classification'] = 'Modes of operation'
my_df.loc[my_df['name'].str.contains('BEAM_INTENSITY'),'classification'] = 'Beam/bunch intensity'
my_df.loc[my_df['name'].str.contains('BUNCH_INTENSITY'),'classification'] = 'Beam/bunch intensity'
my_df.loc[my_df['name'].str.contains('BUNCH_LENGTH'),'classification'] = 'Beam/bunch length'
my_df.loc[my_df['name'].str.contains('NO_BUNCHES'),'classification'] = 'Filling scheme'
my_df.loc[my_df['name'].str.contains('INJECTION_SCHEME'),'classification'] = 'Filling scheme'
my_df.loc[my_df['name'].str.contains('BUNCH_EMITTANCE'),'classification'] = 'Emittances'
my_df.loc[my_df['name'].str.contains('EMITTANCE_SCAN'),'classification'] = 'Emittances'

my_df.loc[my_df['name'].str.contains('LUMISERVER'),'classification'] = 'Luminosity server'
my_df.loc[my_df['name'].str.contains('LhcStateTracker'),'classification'] = 'State tracker'
my_df.loc[my_df['name'].str.contains('-XING-'),'classification'] = 'Crossing angles'
my_df.loc[my_df['name'].str.contains('LUMILEVELING'),'classification'] = 'Luminosity'
my_df.loc[my_df['name'].str.contains('ATLAS:I_MEAS'),'classification'] = 'Experiments magnets'
my_df.loc[my_df['name'].str.contains('ALICE:I_MEAS'),'classification'] = 'Experiments magnets'
my_df.loc[my_df['name'].str.contains('CMS:I_MEAS'),'classification'] = 'Experiments magnets'
my_df.loc[my_df['name'].str.contains('LHCB_POLARITY'),'classification'] = 'Experiments magnets'
my_df.loc[my_df['name'].str.contains('RPTH.SX2.RXSOL.ALICE'),'classification'] = 'Experiments magnets'


my_df[my_df['classification'].isna()]
# %%
# for each group of classification print the number of variables

for ii in np.unique(my_df['classification']):
      print(f'{ii} : {len(my_df[my_df["classification"]==ii])}')
      for jj in my_df[my_df["classification"]==ii]['name']:
            print(f'    {jj}')



# return my_df with classification == NaN
my_df[my_df['classification'].isna()]


# aux.to_json('snapshot_variables.json', orient='records', indent=4)]

# %%
# my_last_fill=sk.get_fill_time('last')
# 9063 is long, 8999 is the longest
for my_fill in [  #9062,
                  #9059,
                  #9057,
                  #9056,
                  #9055,
                  #9050,
                  #9049,
                  #9046,
                  #9045,
                  #9044,
                  #9043,
                  #9036,
                  #9035,
                  #9031,
                  #9029,
                  9023,
                  #9022,
                  #9019,
                  #9017,
                  #9016,
                  #9014,
                  9007]:
      fill_to_parquet(my_fill, my_path=my_path, delete_and_rewrite=False)

# %%
# aux= my_path/'HX:FILLN=9072'
# parquet_path = str(aux.absolute())

# aux = pd.read_parquet(parquet_path, columns=['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'])
# type(aux['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'].dropna().iloc[1])
# # %%
# # careful with dask!!! It changes the data type
# from dask import dataframe as dd
# aux =dd.read_parquet(parquet_path, columns=['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY']).compute()
# type(aux['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'].dropna().iloc[1])

# # %%
# import pyarrow.parquet as pq
# type(pq.read_table(parquet_path).to_pandas()['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'].dropna().iloc[1])
# # 

# # %%

# %%
import pandas as pd

aux=pd.read_parquet('/eos/project/l/lhc-lumimod/LuminosityFollowUp/2023/rawdata/HX:FILLN=8672', columns=['LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY'])
aux.dropna().sort_index().iloc[1]
import matplotlib.pyplot as plt
plt.plot(aux.dropna().iloc[1].values[0])
# %%
