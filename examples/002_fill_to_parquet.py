# %%
from pathlib import Path
import os
import getpass
import logging
import sys
import json
import nx2pd as nx 
import pandas as pd
import time

my_path=Path('/home/lumimod/2022_07_26_NXCALS/2022/')





logging.basicConfig(stream=sys.stdout, level=logging.INFO)

os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
print(f'Assuming that your kerberos keytab is in the home folder, ' 
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/{getpass.getuser()}.keytab {getpass.getuser()}');

# %%

from nxcals.spark_session_builder import get_or_create, Flavor
logging.info('Creating the spark instance')
spark = get_or_create(flavor=Flavor.YARN_LARGE, 
master='yarn', conf={'spark.driver.maxResultSize': '10g'})
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')

# %%

def is_fill_in_parquet(fill, my_path):
      path_test=my_path/f"HX:FILLN={fill}"
      return path_test.exists()

def fill_to_parquet(fill_number):
      if not is_fill_in_parquet(fill_number, my_path):
            try:
                  aux = sk.get_fill_time(fill_number)
                  print(json.dumps(aux,
                              sort_keys=False, indent=4,default=str))

                  my_df = sk.get_fill_data(aux['fill'],
                          event_list=datavars['2022']['event_list'],
                          data_list=datavars['2022']['data_list'],
                          sampling_frequency = None) 
                  my_df['creation time'] = datetime.utcnow() 
                  my_df.loc[aux['start'].value:
                            aux['end'].value].to_parquet(my_path,partition_cols='HX:FILLN')           
            except:
                  print('Fill not found!')
# %%

my_last_fill=sk.get_fill_time('last')
for ii in range(7854, 7855):#my_last_fill['fill']+1):
      print(ii)
      fill_to_parquet(ii)
# %%


