# %%
print( 40*'+' + '\n Assuming you have a valid kerberos token.\n' + 40*'+')

import nx2pd as nx
import pandas as pd
from nx2pd import SparkIt
from nxcals.spark_session_builder import get_or_create, Flavor
spark = get_or_create(flavor=Flavor.LOCAL)

sk = SparkIt(spark)

t0 = pd.Timestamp('2022.11.05 21:00', tz='Europe/Zurich')
t1 = pd.Timestamp('2022.11.06 03:00', tz='Europe/Zurich')

data_list= ['%IP1-SDISP-CORR-XING%value',
            '%IP1-XING-V-MURAD%value',
            '%IP1-XING-H-MURAD%value',
            '%LHCBEAM1%LANDAU_DAMPING%value',
            'RPMC.UL16.RBBCW.R1B2:I_MEAS',
            'LhcStateTracker:State:beamProcess',
            'LhcStateTracker:State:beamProcessTypeCategory',
            'LhcStateTracker:State:opticId',
            'LhcStateTracker:State:opticName',
            'LhcStateTracker:State:secondsInBeamProcess'
            ]

# %%
my_df = sk.get(t0, t1, data_list)

# %%
from matplotlib import pyplot as plt
plt.figure(figsize=(15,10))
plt.plot(my_df['LhcStateTracker:LHCBEAM:IP1-SDISP-CORR-XING:value'].dropna())
plt.plot(my_df['RPMC.UL16.RBBCW.R1B2:I_MEAS'].dropna())
plt.plot(my_df['LhcStateTracker:LHCBEAM:IP1-XING-V-MURAD:value'].dropna())
plt.plot(my_df['LhcStateTracker:LHCBEAM1:LANDAU_DAMPING:value'].dropna())




# %%
for ii in ['LhcStateTracker:State:opticName', 
           'LhcStateTracker:State:beamProcess',
           ]:
  print(ii)
  print(my_df[ii].dropna().tail(10))
  print(80*'=')

# %%
