# %%
"""
### Introduction

If not done already, you can install your python NXCALS setup following
the instruction in
https://codimd.web.cern.ch/1hK2KGDHSBGqzT8JtrwxgQ
"""

# %%
#This is not needeed but convenient if you are modifying/developing
#the modules you are importing
try:
    from IPython import get_ipython
    ipython = get_ipython()
    # If in ipython, load autoreload extension
    if "ipython" in globals():
        ipython.magic("load_ext autoreload")
        ipython.magic("autoreload 2")
        print("Welcome to IPython!")
except:
    print('Welcome to Python!')

# %%
import os
os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"

import sys
import numpy as np
import nx2pd as nx
import pandas as pd
from matplotlib import pyplot as plt
from nx2pd import SparkIt

# %%
# get your kerberos token, it assumes a keytab on your home
nx.get_kerberos_token()

# get your kerberos token, it assumes the "standard" NXCALS installation
from nxcals.spark_session_builder import get_or_create, Flavor

spark = get_or_create(flavor=Flavor.YARN_LARGE, master='yarn')

# %%
# To assert that the python local python version is the same of the drivers
local_info = sys.version_info[:2]
print(f'The python version is {".".join(map(str,local_info))}')
worker_info = (
    spark.sparkContext.parallelize(range(5))
    .map(lambda x: sys.version_info[:2])
    .collect()
)
for ii in worker_info:
    assert ii == local_info

# %%
"""
### The `nxcals_df` method

In this section we are going to show examples of the `nxcals_df` method of the SparkIt class. This is the fundamental method of the class and allows to retrieve the data from NXCALS, namely the `pyspark` or `pandas` dataframes.


It is quite natural to have the variable name and the time windows [t0, t1] and the main arguments of the function. 

#### About timestamps

Data in NXCALS are stored versus time as mostly logging systems.
NXCALS stores timestamp as int64 (long), that is [Unix time](https://en.wikipedia.org/wiki/Unix_time) in ns. The assumed timezone is UTC as for all Unix time. This has the advantage to be monotonous and without missing values (as in the case of CET time-zone).
It is important that t0 and t1 maintain the ns precision, that is the native precision of NXCALS.
"""

# %%
# Note that pd.Timestamp maintains the ns precision
result = pd.Timestamp("2018-04-02 00:00:00.000000001", tz="CET").value
result

# %%
pd.Timestamp(result)

# %%
# loading the SparkIt class and passing the SparkSession handle to the constructor
sk = SparkIt(spark)

# Simplest approach
data_list = ["HX:FILLN"]
t0 = 1634707139157238525
t1 = 1634751674621363525
df = sk.nxcals_df(data_list, t0, t1)
pd_df = df.toPandas()
assert len(pd_df) == 2  # a sanity check
assert df.toPandas().nxcals_value.values[0] == 7505
assert df.toPandas().nxcals_value.values[1] == 7504
df.show()

# %%
data_list = ["HX:FILLN"]
# You can use date strings
t0 = "2021-10-20 05:18:59.157238524"
# you can combine different timezone strings
t1 = "2021-10-20 17:41:14.621363525-0100"
df = sk.nxcals_df(data_list, t0, t1)
df.show()

# %%
data_list = ["HX:FILLN"]
# You can use directly pd.Timestamp
# and mix the different formats
# Note that all format (pd.Timistamp, int, str) maintains the ns precision
t0 = 1634707139157238525
t1 = pd.Timestamp("2021-10-20 17:41:14.621363525", tz="UTC")
df = sk.nxcals_df(data_list, t0, t1)
df.show()

# %%
# Warning! A typo problem is silent...
data_list = ["HX:FILLN_THIS_IS_A_TYPO"]  # NB: this is a typo....
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-21", tz="UTC")
df = sk.nxcals_df(data_list, t0, t1)
df.show()

# %%
"""
#### About variables 

In NXCALS the variable are more sofisticated abstractions than in CALS.  So we will present them by examples.
We will discuss variables, entities, and the differnt data systems in CALS (e.g., Massi files and LSA).

"""

# %%
# Using two variables and the % operators
data_list = ["HX:FILLN", "LHC.BCTDC%:BEAM_INTENSITY"]
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-21", tz="UTC")
df = sk.nxcals_df(data_list, t0, t1)
df.show()

# %%
# BUT the list of variables should be of the same type,
# 'HX:FILLN' is a number
# 'HX:BMODE' is a string
# so the following raise an error
# cern.nxcals.api.extraction.data.exceptions.IncompatibleSchemaPromotionException

data_list = ["HX:FILLN", "HX:BMODE"]
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-21", tz="UTC")
try:
    df = sk.nxcals_df(data_list, t0, t1)
    df.show()
except Exception as e:
    print(e)

# %%
"""
Now we can show how to access entities. An entity has differnt parameters, in the example below we have "source", "signal" and "version". In addition we need also to consider a differnt "system_name". To address the need for this flexibility we consider dictionairies as input for the `nxcals_df` method.
"""

# %%
# we can access entities like the Massi files:
# see https://indico.cern.ch/event/1077837
t0 = 1524246988000000000
t1 = t0 + 100e9
data_list = [
    {"source": "CMS.OFFLINE", "signal": "LUMI_TOT_INST", "version": "4.5"},
    {"source": "CMS.OFFLINE", "signal": "LUMI_TOT_INST", "version": "4.6"},
]
df = sk.nxcals_df(data_list, t0, t1, system_name="LHC_OFFLINE")

# converted to pandas for readibility
df.toPandas()

# %%
# Another example
t0 = 1532316952557000000
t1 = t0 + 5e9
data_list = [
    {
        "context": "PHYSICS-6.5TeV-30cm-120s-2018_V1@120_[END]",
        "parameter": "LHCBEAM%/IP%_SEPSCAN_%_MM",
    }
]
df = sk.nxcals_df(data_list, t0, t1, system_name="SETTINGS")

# converted to pandas for readibility
df.toPandas()

# %%
"""
### Spark processing
All the previous example show simple example of extractions without data processing.
The convenience of spark/NXCALS is to aggregate data via postprocessing at the source so that we can use the spark server and the data locality, and then retrive the aggregated data. 
We worked on some interface that allow it, in particular to do subsampling in NXCALS. In the following we are going to make some examples.
"""

# %%
data_list = ["LHC.BCTDC%:BEAM_INTENSITY"]
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-21", tz="UTC")
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="5min",
            index="nxcals_timestamp",
            columns="nxcals_variable_name",
            value="nxcals_value",
            rounding="1min",
            verbose=False,
        )
    ],
)
df.show()

# %%
# it is working also with the VECTORNUMERIC
data_list = ["LHC.BCTFR.%.B%:BUNCH_INTENSITY"]
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-20 01:00", tz="UTC")
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="5min",
            index="nxcals_timestamp",
            columns="nxcals_variable_name",
            value="nxcals_value",
            rounding="1min",
            verbose=False,
        )
    ],
)
df.show()

# %%
# we can access entities like the Massi files:
# see https://indico.cern.ch/event/1077837
t0 = 1524246988000000000
t1 = t0 + 1000e9
data_list = [{"source": "CMS.OFFLINE", "signal": "LUMI_TOT_INST", "version": "4.5"}]
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    system_name="LHC_OFFLINE",
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="5min",
            index="__record_timestamp__",
            columns="signal",
            value="value",
            rounding="1min",
            verbose=False,
        )
    ],
)

# converted to pandas for readibility
df.toPandas()

# %%
# Another example
t0 = 1532316952557000000
t1 = t0 + 5e9
data_list = [
    {
        "context": "PHYSICS-6.5TeV-30cm-120s-2018_V1@120_[END]",
        "parameter": "LHCBEAM%/IP%_SEPSCAN_%_MM",
    }
]
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    system_name="SETTINGS",
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="5min",
            index="__record_timestamp__",
            columns="parameter",
            value="value",
            rounding="1min",
            verbose=False,
        )
    ],
)

# converted to pandas for readibility
df.toPandas()

# %%
"""
### Pandas processing
Onece the data is aggregate in a spark dataframe we can transform it with some local postprocessing. In the following, few examples.
"""

# %%
data_list = ["LHC.BCTDC%:BEAM_INTENSITY"]
t0 = pd.Timestamp("2021-10-20", tz="UTC")
t1 = pd.Timestamp("2021-10-21", tz="UTC")
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="1min",
            index="nxcals_timestamp",
            columns="nxcals_variable_name",
            value="nxcals_value",
            rounding="1min",
            verbose=False,
        )
    ],
    pandas_processing=[nx.pandas_get, nx.pandas_pivot, nx.pandas_index_localize],
)
df

# %%
# Another example
t0 = 1532316952557000000
t1 = t0 + 30 * 24 * 3600e9
data_list = [
    {
        "context": "PHYSICS-6.5TeV-30cm-120s-2018_V1@120_[END]",
        "parameter": "LHCBEAM%/IP%_SEPSCAN_%_MM",
    }
]
df = sk.nxcals_df(
    data_list,
    t0,
    t1,
    system_name="SETTINGS",
    spark_processing=[
        lambda data: nx.spark_subsampling(
            data,
            start=t0,
            end=t1,
            freq="5min",
            index="__record_timestamp__",
            columns="parameter",
            value="value",
            rounding="1min",
            verbose=False,
        )
    ],
    pandas_processing=[
        nx.pandas_get,
        lambda data: nx.pandas_pivot(
            data, index="__record_timestamp__", columns="parameter", values="value"
        ),
        nx.pandas_index_localize,
    ],
)

df

# %%
"""
Now let assume that we end-up with two dataframes. One with events and one with measurements (subsampling an event is not very useful as subsampling a measurement).
"""

# %%
# Simplest approach
data_list = ["HX:BMODE"]
t0 = 1634707139157238525
t1 = 1634751674621363524
df1 = sk.nxcals_df(
    data_list,
    t0,
    t1,
    pandas_processing=[nx.pandas_get, nx.pandas_pivot],
)
df1

# %%
data_list = ["LHC.BCTFR.%.B%:BUNCH_INTENSITY"]
df2 = sk.nxcals_df(
    data_list,
    t0,
    t1,
    spark_processing=[
        lambda data: nx.spark_subsampling(data, start=t0, end=t1, freq="10min")
    ],
    pandas_processing=[nx.pandas_get, nx.pandas_pivot],
)
df2

# %%
# we can concatenate
df = pd.concat([df1, df2]).sort_index()

# %%
# and ffill the event(s)
nx.pandas_index_localize(nx.pandas_ffill([df[["HX:BMODE"]]]))

# %%
# and interpolate the
nx.pandas_index_localize(
    nx.pandas_interpolate(df[["LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY"]])
)
# NB: the interpolation is working for the moment only with Unix time.

# %%
"""
With these basic blocks we have a syntax to build the subsampling and interpolations in the different dataframes.
"""

# %%
print('All is done!')