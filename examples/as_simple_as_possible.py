# %%
# we assume you have a valid kerberos token)
import nx2pd as nx
from nxcals.spark_session_builder import get_or_create, Flavor
spark = get_or_create(flavor=Flavor.LOCAL)
sk  = nx.SparkIt(spark)

# %%
# the 'get' function
import pandas as pd
t1 = pd.Timestamp.now(tz="UTC")
t0 = t1-pd.Timedelta('1h')
df = sk.get(t0, t1, ['LHC.BCTDC%:BEAM_INTENSITY'])
df
# %%
# the 'get_variables' function
import json
aux = sk.get_variables(['LHC.BCTDC%:BEAM_INTENSITY'])
print(json.dumps(aux,
                 sort_keys=False, indent=4, default=str))
# %%
# the 'get_fill_time' function
my_fill = 9072
aux = sk.get_fill_time(my_fill)
print(json.dumps(sk.get_fill_time(my_fill),
                 sort_keys=False, indent=4,default=str))
# %%
# the 'get_fill_raw_data' function
aux = sk.get_fill_raw_data(9072,['LHC.BCTDC%:BEAM_INTENSITY'])
aux
# %%
# the 'fill_to_parquet' function
from pathlib import Path
current_directory = Path.cwd()
sk.fill_to_parquet(9072, current_directory, [['HX:FILLN', 'HX:BMODE']], [['LHC.BCTDC%:BEAM_INTENSITY']], split_h=100)

# %%
# using snapshot
variables_info, time_info = sk.get_snapshot('2023 Lumi Follow-Up')
# %%
