# nx2pd example


### Installation on a Unix-like machine

If you are installing a miniconda from scrath on an CERN unix-like machine please do
```bash

! STEP 0
! go to a test/favorite folder
cd my_folder

! STEP 1
! clone this repo 
git clone ssh://git@gitlab.cern.ch:7999/lhclumi/nx2pd.git

! STEP 2
! install the new miniconda
source ./nx2pd/make_it.sh

! STEP 3 
! install the requirements
python -m pip install -r ./nx2pd/requirements.txt

! STEP 4
! install the 
python -m pip install ./nx2pd

! STEP 5
! `kinit` with your account and `aklog`

! STEP 6
! you should be able to run 
! at the first run can be a bit slow since a venv has to be created
! the errors/warnings will be redirected to log.txt 
python ./nx2pd/examples/001_simple.py 2> log.txt

! FURTHER STEPS
! have a look inside ./nx2pd/examples/001_simple.py 
! to have an overview of the rationale/interface.
```

### Installation on SWAN

Git clone this repository on your `eos` (e.g. via lxplus), then go to SWAN and start the NXCALS software stack. Open the SWAN terminal and go on the eos repository folder. Then install it with

`pip install --user .`

See https://swan.docs.cern.ch/advanced/install_packages/ for more details.

Then you open a notebook in SWAN and connect to Spark (clicking on the 'star' button and typing your NICE password). Then you can use the nx2pd in your notebook:

```
import json
from nx2pd import SparkIt
sk = SparkIt(spark) 

my_fill = 6666
aux = sk.get_fill_time(my_fill)
print(json.dumps(sk.get_fill_time(my_fill),
                 sort_keys=False, indent=4,default=str))
# you have more example on the examples folder in the repository
```