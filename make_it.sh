# go to the folder you want to use

# from http://bewww.cern.ch/ap/acc-py/installers/   
# using the 3.9 version from https://docs.conda.io/projects/miniconda/en/latest/miniconda-other-installer-links.html                           
wget https://repo.anaconda.com/miniconda/Miniconda3-py39_23.9.0-0-Linux-x86_64.sh        
bash Miniconda3-py39_23.9.0-0-Linux-x86_64.sh -b  -p ./miniconda -f                       
source miniconda/bin/activate                                                      
#python -m venv ./venv                                                              
#source ./venv/bin/activate                                                         

# from https://nxcals-docs.web.cern.ch/current/user-guide/data-access/quickstart/#instalation-nxcals-package-from-acc-py-repository
python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch nxcals
