'''
A simple package to convert NX CERN Logging information into a pandas dataframe.
'''
__version__ = "v0.2"

from .nx2pd import *
from .nx2pd import SparkIt
